[metadata]
name = castellum
version = attr: castellum.__version__
author = Max-Planck-Gesellschaft
author_email = castellum@mpib-berlin.mpg.de
license = AGPL-3.0-or-later
url = https://git.mpib-berlin.mpg.de/castellum/castellum

[options]
include_package_data = True
packages = find:
install_requires =
    castellum-pseudonyms == 0.0.1
    cologne-phonetics == 1.3.1
    Django == 3.2.16
    django-bootstrap5 == 22.2
    django-mfa3 == 0.8.0
    django-npm == 1.0.0
    django-parler == 2.3
    django-phonenumber-field == 7.0.1
    django-stronghold == 0.4.0
    Faker == 15.3.4
    geopy == 2.2.0
    icalendar == 5.0.3
    icalevents == 0.1.25
    jsonschema == 4.17.3
    phonenumberslite == 8.13.2
    python-dateutil == 2.8.2
    requests == 2.28.1
    simplecharts == 0.0.2

[options.extras_require]
test =
    pytest
    pytest-cov
    pytest-django
    pytest-freezegun
    django-test-migrations
    model-bakery
    lxml
    flake8 < 6.0.0
    flake8-sfs
    flake8-print
    flake8-copyright
    isort
    pospell
dev =
    ipdb
    django-debug-toolbar

[tool:pytest]
addopts =
    --ds=castellum.settings.development
    --cov=castellum
    --cov-branch
    --cov-report term-missing:skip-covered
    --cov-report html
    --no-cov-on-fail
    --strict-markers
markers =
    unittest: marks tests as fast. They test only one function and don't need access to the database
    smoketest: simple tests that only check for 200 status code
testpaths = tests

[flake8]
exclude =
    .venv,
    .git,
    .tox,
    migrations,
    settings,
    docs,
    node_modules,
max-line-length = 100
extend-ignore = SFS201
max-complexity = 8
copyright-check = True
copyright-regexp = \(c\) 2018-2023 MPIB
per-file-ignores =
    tests/*:C801
    castellum/asgi.py:C801
    castellum/wsgi.py:C801
    castellum/*/__init__.py:C801
    setup.py:C801
    manage.py:C801

[isort]
known_django = django
sections = STDLIB,DJANGO,THIRDPARTY,FIRSTPARTY,LOCALFOLDER
force_single_line = True
skip = migrations
line_length = 100

[coverage:report]
omit =
    manage.py
    castellum/wsgi.py
    castellum/settings/*
    castellum/*/migrations/*.py
skip_covered = True
skip_empty = True
