<?php
@define('CONST_Postgresql_Version', '11');
@define('CONST_Postgis_Version', '2.5');
@define('CONST_Website_BaseURL', '/');
@define('CONST_Replication_Url', 'https://download.geofabrik.de/europe/germany/berlin-updates');
@define('CONST_Replication_MaxInterval', '86400');
@define('CONST_Replication_Update_Interval', '86400');
@define('CONST_Replication_Recheck_Interval', '900');
@define('CONST_Pyosmium_Binary', '/usr/local/bin/pyosmium-get-changes');
