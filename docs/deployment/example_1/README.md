# Example 1: Containerized Deployment

This section of the deployment documentation illustrates how to set up
Castellum based on Docker, uWSGI, PostgreSQL, and LDAP.

Simply open a terminal and launch `start.sh`. Pass `--demo` to
automatically create demo content.
