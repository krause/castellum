# Get in touch

For most kinds of contributions, the best first step is to contact us at
<castellum@mpib-berlin.mpg.de>. If you plan to to contribute regularly,
we can create an account for you on our gitlab server.

# Reporting bugs

When reporting an issue, please make sure to answer these questions:

1. What did you do?
2. What did you expect to see?
3. What did you see instead?
4. Which version of castellum are you using?
5. Which browser (in which version) are you using?

If you find a security vulnerability, please do **not** open a public
issue. Email <infosec@mpib-berlin.mpg.de> instead.

# Feature requests

While we are a rather small project, we would be happy to hear about
your ideas!

However, Please understand that our mission is first and foremost to
serve the Max Planck Gesellschaft and its institutes. While we try to
build a reusable product, we cannot implement every potentially useful
feature request.

# Contributing code

Please make sure to run all tests before submitting the changes. You
should also include tests for new features.

All code will be reviewed by a member of the core team before it is
merged into the main branch. The reviewers will probably leave some
comments and requests. Please respond to those within two weeks. If we
do not hear back from you we will expect you lost interest.

For further information on the castellum code and its structure see
`docs/development.md`.
