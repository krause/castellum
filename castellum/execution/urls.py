# (c) 2018-2023 MPIB <https://www.mpib-berlin.mpg.de/>,
#     2018-2019 MPI-CBS <https://www.cbs.mpg.de/>,
#     2018-2019 MPIP <http://www.psych.mpg.de/>
#
# This file is part of Castellum.
#
# Castellum is free software; you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Castellum is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with Castellum. If not, see
# <http://www.gnu.org/licenses/>.

from django.conf import settings
from django.urls import path

from .api import APIAttributesView
from .api import APIDomainsView
from .api import APIPseudonymsView
from .api import APIResolveView
from .api import APIValidateView
from .feeds import AppointmentFeedForStudy
from .feeds import AppointmentFeedForUser
from .feeds import ResourceFeed
from .views import AdditionalInfoUpdateView
from .views import AttributesUpdateView
from .views import CalendarView
from .views import ConsentChangeView
from .views import ContactUpdateView
from .views import DataProtectionUpdateView
from .views import ExclusionCriteriaView
from .views import ExportView
from .views import LegalRepresentativeContactUpdateView
from .views import NewsMailView
from .views import ParticipationAppointmentsUpdateView
from .views import ParticipationConsentUpdateView
from .views import ParticipationConsentView
from .views import ParticipationDetailView
from .views import ParticipationDropoutConfirmView
from .views import ParticipationDropoutView
from .views import ParticipationListView
from .views import ParticipationPseudonymsView
from .views import ParticipationPseudonymView
from .views import ParticipationUpdateView
from .views import ProgressView
from .views import ResolveView
from .views import TagView

app_name = 'execution'
urlpatterns = [
    path('calendar/feed/', AppointmentFeedForUser.as_view(), name='calendar-user-feed'),
    path('resources/<int:pk>/', ResourceFeed.as_view(), name='resource-feed'),
    path('<int:study_pk>/', ParticipationListView.as_view(), name='participation-list'),
    path('<int:study_pk>/resolve/', ResolveView.as_view(), name='resolve'),
    path('<int:study_pk>/news/', NewsMailView.as_view(), name='news-mail'),
    path('<int:pk>/calendar/', CalendarView.as_view(), name='calendar'),
    path('<int:pk>/calendar/feed/', AppointmentFeedForStudy.as_view(), name='calendar-feed'),
    path('<int:pk>/criteria/', ExclusionCriteriaView.as_view(), name='criteria'),
    path('<int:pk>/export/', ExportView.as_view(), name='export'),
    path('<int:pk>/progress/', ProgressView.as_view(), name='progress'),
    path('<int:pk>/tags/', TagView.as_view(), name='execution-tags'),
    path(
        '<int:study_pk>/<int:pk>/',
        ParticipationDetailView.as_view(),
        name='participation-detail',
    ),
    path(
        '<int:study_pk>/<int:pk>/pseudonyms/',
        ParticipationPseudonymsView.as_view(),
        name='participation-pseudonyms',
    ),
    path(
        '<int:study_pk>/<int:pk>/pseudonyms/<domain>/',
        ParticipationPseudonymView.as_view(),
        name='participation-pseudonym',
    ),
    path(
        '<int:study_pk>/<int:pk>/dropout/',
        ParticipationDropoutView.as_view(),
        name='participation-dropout',
    ),
    path(
        '<int:study_pk>/<int:pk>/dropout/confirm/',
        ParticipationDropoutConfirmView.as_view(),
        name='participation-dropout-confirm',
    ),
    path(
        '<int:study_pk>/<int:pk>/update/',
        ParticipationUpdateView.as_view(),
        name='participation-update',
    ),
    path(
        '<int:study_pk>/<int:pk>/consent/',
        ParticipationConsentView.as_view(),
        name='participation-consent',
    ),
    path(
        '<int:study_pk>/<int:pk>/consent/change/',
        ParticipationConsentUpdateView.as_view(),
        name='participation-consent-change',
    ),
    path(
        '<int:study_pk>/<int:pk>/appointments/',
        ParticipationAppointmentsUpdateView.as_view(),
        name='participation-appointments',
    ),
    path(
        '<int:study_pk>/<int:pk>/update-contact/',
        ContactUpdateView.as_view(),
        name='contact-update',
    ),
    path(
        '<int:study_pk>/<int:pk>/update-attributes/',
        AttributesUpdateView.as_view(),
        name='attributes-update',
    ),
    path(
        '<int:study_pk>/<int:pk>/update-data-protection/',
        DataProtectionUpdateView.as_view(),
        name='data-protection-update',
    ),
    path(
        '<int:study_pk>/<int:pk>/update-data-protection/consent/',
        ConsentChangeView.as_view(),
        name='consent-change',
    ),
    path(
        '<int:study_pk>/<int:pk>/update-additional-info/',
        AdditionalInfoUpdateView.as_view(),
        name='additional-info-update',
    ),
    path(
        '<int:study_pk>/<int:participation_pk>/representatives/<hash>/update-contact/',
        LegalRepresentativeContactUpdateView.as_view(),
        name='legal-representative-contact-update',
    ),
]

if settings.CASTELLUM_API_ENABLED:
    urlpatterns += [
        path('api/studies/<int:pk>/domains/', APIDomainsView.as_view()),
        path('api/studies/<int:pk>/domains/<uuid:domain>/', APIPseudonymsView.as_view()),
        path('api/studies/<int:pk>/domains/<uuid:domain>/<pseudonym>/', APIValidateView.as_view()),
        path(
            'api/studies/<int:study_pk>/domains/<uuid:domain>/<pseudonym>/attributes/',
            APIAttributesView.as_view(),
        ),
        path(
            'api/studies/<int:study_pk>/domains/<uuid:domain>/<pseudonym>/<uuid:target_domain>/',
            APIResolveView.as_view(),
        ),
    ]
