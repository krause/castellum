# Generated by Django 3.2.10 on 2021-12-13 13:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recruitment', '0039_subject_uuid_foreign_key'),
        ('pseudonyms', '0006_subject_uuid_foreign_key'),
    ]

    operations = [
        migrations.AddField(
            model_name='domain',
            name='exportable_attributes',
            field=models.ManyToManyField(blank=True, related_name='_pseudonyms_domain_exportable_attributes_+', to='recruitment.AttributeDescription', verbose_name='Exportable recruitment attributes'),
        ),
    ]
