# (c) 2018-2023 MPIB <https://www.mpib-berlin.mpg.de/>,
#     2018-2019 MPI-CBS <https://www.cbs.mpg.de/>,
#     2018-2019 MPIP <http://www.psych.mpg.de/>
#
# This file is part of Castellum.
#
# Castellum is free software; you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Castellum is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with Castellum. If not, see
# <http://www.gnu.org/licenses/>.

from django.conf import settings
from django.contrib.gis.db import models
from django.core import checks

from castellum.contacts.models import Contact


class Geolocation(models.Model):
    point = models.PointField()
    contact = models.OneToOneField(Contact, on_delete=models.CASCADE)


@checks.register()
def nominatim_check(app_configs, **kwargs):
    errors = []
    if not hasattr(settings, 'NOMINATIM') and not settings.DEBUG:
        errors.append(
            checks.Info(
                'NOMINATIM not configured; geocoding is disabled',
                id='geofilters.W001',
            )
        )
    return errors
