# Generated by Django 3.2.8 on 2021-10-20 15:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('recruitment', '0038_subject_uuid'),
        ('subjects', '0031_subject_uuid_primary_key'),
    ]

    operations = [
        migrations.AlterField(
            model_name='participation',
            name='subject_uuid',
            field=models.ForeignKey(editable=False, on_delete=django.db.models.deletion.CASCADE, to='subjects.subject'),
        ),
        migrations.RenameField(
            model_name='participation',
            old_name='subject_uuid',
            new_name='subject',
        ),
        migrations.AlterUniqueTogether(
            name='participation',
            unique_together={('study', 'subject')},
        ),

        migrations.AlterField(
            model_name='reliabilityentry',
            name='subject_uuid',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='subjects.subject', verbose_name='Subject'),
        ),
        migrations.RenameField(
            model_name='reliabilityentry',
            old_name='subject_uuid',
            new_name='subject',
        ),
    ]
