# (c) 2018-2023 MPIB <https://www.mpib-berlin.mpg.de/>,
#     2018-2019 MPI-CBS <https://www.cbs.mpg.de/>,
#     2018-2019 MPIP <http://www.psych.mpg.de/>
#
# This file is part of Castellum.
#
# Castellum is free software; you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Castellum is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with Castellum. If not, see
# <http://www.gnu.org/licenses/>.

import uuid

from django.core.management.base import BaseCommand

from castellum.recruitment.attribute_exporters import get_exporter
from castellum.recruitment.models import AttributeDescription
from castellum.subjects.models import Subject


class Command(BaseCommand):
    help = 'Export recruitment attributes for a single subject or the attribute schema.'

    def add_arguments(self, parser):
        parser.add_argument('subject_uuid', type=uuid.UUID, nargs='?')
        parser.add_argument('--exporter')

    def handle(self, **options):
        exporter = get_exporter(options.get('exporter'))
        descriptions = AttributeDescription.objects.all()
        if options['subject_uuid'] is None:
            s = exporter.get_schema(descriptions)
        else:
            subject = Subject.objects.get(pk=options['subject_uuid'])
            s = exporter.get_data(descriptions, [(subject.pk, subject)])
        self.stdout.write(s)
