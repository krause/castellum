# Generated by Django 3.2.9 on 2021-11-16 14:15

import castellum.utils.fields
import django.core.serializers.json
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    replaces = [
        ('subjects', '0001_squashed_0018_subject_slug'),
        ('subjects', '0019_subject_additional_suitability_document'),
        ('subjects', '0020_subject_deceased'),
        ('subjects', '0021_merge_attributeset'),
        ('subjects', '0022_verbose_names'),
        ('subjects', '0023_subject_blocked'),
        ('subjects', '0024_subject_rm_note_fields'),
        ('subjects', '0025_rm_availability'),
        ('subjects', '0026_subjectcreationrequest'),
        ('subjects', '0027_rename_subject_slug_to_uuid'),
        ('subjects', '0028_alter_subject_uuid'),
    ]

    initial = True

    dependencies = [
        ('studies', '0001_squashed_0035_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ConsentDocument',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('is_valid', models.BooleanField(default=True, verbose_name='Is valid')),
                ('is_deprecated', models.BooleanField(default=False, verbose_name='Is deprecated')),
                ('file', castellum.utils.fields.RestrictedFileField(blank=True, upload_to='consent/', verbose_name='File')),
            ],
            options={
                'get_latest_by': 'created_at',
            },
        ),
        migrations.CreateModel(
            name='Subject',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateField(auto_now_add=True, verbose_name='Created at')),
                ('updated_at', models.DateField(auto_now=True, verbose_name='Updated at')),
                ('uuid', models.UUIDField(default=uuid.uuid4, unique=True, verbose_name='UUID')),
                ('attributes', models.JSONField(encoder=django.core.serializers.json.DjangoJSONEncoder, verbose_name='Recruitment attributes')),
                ('onetime_invitation_disinterest', models.BooleanField(default=False, verbose_name='Does not want to participate in one time invitations')),
                ('privacy_level', models.PositiveIntegerField(choices=[(0, '0 (regular)'), (1, '1 (increased)'), (2, '2 (high)')], default=0, verbose_name='Privacy level')),
                ('to_be_deleted', models.DateField(blank=True, help_text='All data about this subject should be deleted or fully anonymized. This includes all data in Castellum and all data collected in studies. This option should be used when a subject requests GDPR deletion.', null=True, verbose_name='To be deleted')),
                ('to_be_deleted_notified', models.BooleanField(default=False)),
                ('export_requested', models.DateField(blank=True, help_text='The subject wants to receive an export of all data we have stored about them. This includes all data in Castellum and all data collected in studies. This option should be used when a subject requests GDPR export.', null=True, verbose_name='Export requested')),
                ('deceased', models.BooleanField(default=False, verbose_name='Subject is deceased')),
                ('blocked', models.BooleanField(default=False, help_text='The person has been blocked due to inappropriate behaviour. They will neither be considered for recruitment, nor contacted as a legal representative. Blocking this person does not automatically block any represented subjects nor co-representatives. They may thus still receive information about the recruitment process.', verbose_name='Subject is blocked')),
                ('source', models.CharField(blank=True, max_length=128, verbose_name='Data source')),
                ('not_available_until', castellum.utils.fields.DateTimeField(blank=True, null=True, verbose_name='not available until')),
                ('additional_suitability_document', models.ManyToManyField(blank=True, help_text='Additional suitability documents are records that certify that a subject may safely undergo all types of exams or tests conducted in the respective type of study.', related_name='_subjects_subject_additional_suitability_document_+', to='studies.StudyType', verbose_name='Additional suitability document for study type')),
                ('study_type_disinterest', models.ManyToManyField(blank=True, related_name='_subjects_subject_study_type_disinterest_+', to='studies.StudyType', verbose_name='Does not want to participate in the following study types')),
            ],
            options={
                'verbose_name': 'Subject',
                'permissions': [('export_subject', 'Can export all data related to a subject')],
            },
        ),
        migrations.CreateModel(
            name='SubjectCreationRequest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateField(auto_now_add=True, verbose_name='Created at')),
                ('updated_at', models.DateField(auto_now=True, verbose_name='Updated at')),
                ('delete_url', models.URLField(blank=True, max_length=128, null=True, unique=True, verbose_name='Delete URL')),
                ('source', models.CharField(blank=True, max_length=128, verbose_name='Data source')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='SubjectNote',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('content', models.CharField(blank=True, max_length=128, verbose_name='Content')),
                ('subject', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='subjects.subject')),
            ],
        ),
        migrations.CreateModel(
            name='ExportAnswer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateField(auto_now_add=True, verbose_name='Created at')),
                ('created_by', models.CharField(max_length=150, verbose_name='Created by')),
                ('subject', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='subjects.subject')),
            ],
            options={
                'verbose_name': 'Export answer',
                'verbose_name_plural': 'Export answers',
            },
        ),
        migrations.CreateModel(
            name='Consent',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Updated at')),
                ('status', models.CharField(choices=[('waiting', 'Waiting for confirmation'), ('confirmed', 'Confirmed')], max_length=64, verbose_name='Status')),
                ('document', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='subjects.consentdocument', verbose_name='Document')),
                ('subject', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='subjects.subject')),
            ],
            options={
                'verbose_name': 'Consent',
                'get_latest_by': 'document__created_at',
            },
        ),
    ]
